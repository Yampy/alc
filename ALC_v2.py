import cv2
import numpy as np
import msvcrt
import os
import time as tm
from collections import deque
import csv
from datetime import datetime

from PhidgetClass import LED
from ToshibaClass import toshibaImage
from ToshibaSerialCCU import ToshibaCCU


def getDateTime():
    return str(datetime.now())


class ALC:
    def __init__(self):
        self.led = LED()
        self.ccu = ToshibaCCU()
        self.vid = toshibaImage(1)
        self.ccu.SetShutterMode(2)

        d_len = 40
        self.img_pv_mem = deque(maxlen=d_len)
        self.img_sat_mem = deque(maxlen=d_len)
        self.img_error_target_mem = deque(maxlen=d_len)
        self.img_error_sat_mem = deque(maxlen=d_len)
        self.alc_error_p_mem = deque(maxlen=d_len)
        self.alc_error_i_mem = deque(maxlen=d_len)
        self.alc_pid_mem = deque(maxlen=d_len)
        self.aec_error_p_mem = deque(maxlen=d_len)
        self.aec_error_i_mem = deque(maxlen=d_len)
        self.aec_pid_mem = deque(maxlen=d_len)
        # self.led_control_mem = deque(maxlen=d_len)
        self.led_setting_mem = deque(maxlen=d_len)
        # self.ccu_control_mem = deque(maxlen=d_len)
        self.ccu_setting_mem = deque(maxlen=d_len)

        # Target and run condition
        self.sample = 0
        self.setpoint = 120
        self.run = True

        # Keeps integral term from running away if CCU and LED are at limits!
        self.ccu_pos_limit = True
        self.ccu_neg_limit = False
        self.led_pos_limit = True
        self.led_neg_limit = False
        self.ccu_pos_lim = 218
        self.ccu_neg_lim = 10
        self.led_pos_lim = 10
        self.led_neg_lim = 0.5

        # Initial LED/CCU settings
        self.led_setting = 10
        self.ccu_setting = 218
        self.ccu.SetShutterSync(self.ccu_setting)
        self.led.setVoltage(0, self.led_setting)

        # Initial Error settings for primer
        self.alc_error_p = 0
        self.alc_error_i = 0
        self.alc_error_d = 0
        self.alc_pid = 0
        self.aec_error_p = 0
        self.aec_error_i = 0
        self.aec_error_d = 0
        self.aec_pid = 0

        # Light/Exposure initials
        self.switch = True  # fixed exposure if True, fixed led if False
        self.sat_weight = 240
        self.b_sat = 920

        self.alc_p = 0.25
        self.alc_i = 0.001
        self.alc_d = 0.0025
        self.aec_p = 0.25
        self.aec_i = 0  # 0.001
        self.aec_d = 0.0025

        self.last_time_sample = tm.time()

        # other useless inits
        self.img = None
        self.img_pv = 0
        self.img_sat = 0
        self.img_error_target = 0
        self.img_error_sat = 0
        self.primer = True

        # logfile init
        if not os.path.isfile('alc_log.csv'):
            self.f = csv.writer(open('alc_log.csv', 'w', newline=''))
            self.f.writerow(
                ['datetime', ' sample', 'sp', 'pv', 'e_target', 'e_sat', 'b_sat', 'sat_weight', 'alc', 'aec', 'led',
                 'led_max', 'led_min', 'ccu', 'ccu_max', 'ccu_min', 'alc_p', 'alc_i', 'alc_d', 'aec_p', 'aec_i',
                 'aec_d', 'alc_error_p', 'alc_error_i', 'alc_error_d', 'aec_error_p', 'aec_error_i', 'aec_error_d'])
        else:
            self.f = csv.writer(open('alc_log.csv', 'a', newline=''))

    def __del__(self):

        self.led.close()
        self.ccu.close()
        self.vid.close()
        print('Comms Closed')

    def getImage(self):

        self.img = self.vid.getImage()
        self.img_show = self.img[40:400, 40:400]
        cv2.rectangle(self.img_show, (30, 30), (330, 330), (255, 255, 0), 2)
        self.img = cv2.cvtColor(self.img_show, cv2.COLOR_BGR2GRAY)
        self.img = self.img[30:330, 30:330]

    def pv_switch(self):

        # Get PV (0-255)
        self.img_pv = np.round(np.mean(self.img), 3)
        # Calculate number of sat pixels
        self.img_sat = len(np.where(self.img == 255)[0])

        # Calculate image and saturation error
        self.img_error_target = (self.setpoint - self.img_pv) / self.setpoint
        self.img_error_sat = -self.sat_weight * self.img_sat / (self.img_pv * pow(self.b_sat - self.img_pv, 2))

        # if operating in ALC, fixed exposure

        # Make sure at least 2 error samples in memory before adjusting controls
        self.primer = len(self.img_pv_mem) > 10
        if self.primer:

            # Fixed Exposure
            if self.switch:
                self.alc_pid_calc()

                # LED is dimmest and brightness target is less than PV
                if self.led_setting == self.led_neg_lim and self.img_error_target < 0:
                    # If exposure not fully closed, switch to fixed lighting, liquid exposure
                    if not self.ccu_neg_limit:
                        self.switch = False
                    self.led_neg_limit = True
                    self.led_pos_limit = False

                # LED if up, and brightness target is more than PV
                elif self.led_setting == self.led_pos_lim and self.img_error_target > 0:
                    # If exposure not fully open, switch to fixed lighting, liquid exposure
                    if not self.ccu_pos_limit:
                        self.switch = False
                    self.led_neg_limit = False
                    self.led_pos_limit = True

                # Keep toggling LED
                else:
                    self.led_neg_limit = False
                    self.led_pos_limit = False

            # Fixed LED
            else:
                self.aec_pid_calc()

                error_reversal_1 = self.img_error_target_mem[-1] > 0 and self.img_error_target < 0
                error_reversal_2 = self.img_error_target_mem[-1] < 0 and self.img_error_target > 0
                error_reversal_3 = self.img_error_target_mem[-1] == self.img_error_target
                error_reversal_4 = self.ccu_setting_mem[-1] == self.ccu_setting

                if self.ccu_setting == self.ccu_pos_lim:
                    self.ccu_pos_limit = True
                    self.ccu_neg_limit = False
                    self.switch = True
                    print('Switching to LED 1')

                elif self.ccu_setting == self.ccu_neg_lim:
                    self.ccu_neg_limit = True
                    self.ccu_pos_limit = False
                    self.switch = True
                    print('Switching to LED 2')

                # if CCU exposure is increasing and suddenly image wants less light, switch back to LED
                elif error_reversal_1 or error_reversal_2 or error_reversal_3 or error_reversal_4:
                    self.switch = True
                    print('Switching to LED 3')

                # Keep toggling CCU
                else:
                    self.ccu_pos_limit = False
                    self.ccu_neg_limit = False
                    print('TOGGLING CCU')

    def alc_pid_calc(self):

        self.alc_error_p = self.img_error_target + self.img_error_sat
        self.alc_error_i = 0.5 * self.alc_error_i_mem[-1] + self.alc_error_p
        self.alc_error_d = self.alc_error_p - self.alc_error_p_mem[-1]
        self.alc_pid = self.alc_p * self.alc_error_p + self.alc_i * self.alc_error_i + self.alc_d * self.alc_error_d

        # Set LED resolution to 1 mV and add to current setting
        self.led_setting = np.round((1 + self.alc_pid) * self.led_setting_mem[-1], 3)

        if self.led_setting > self.led_pos_lim:
            self.led_setting = self.led_pos_lim
        elif self.led_setting < self.led_neg_lim:
            self.led_setting = self.led_neg_lim

        # Update CCU and LED
        self.led.setVoltage(0, self.led_setting)

    def aec_pid_calc(self):

        self.aec_error_p = self.img_error_target + self.img_error_sat
        self.aec_error_i = 0.5 * self.aec_error_i_mem[-1] + self.aec_error_p
        self.aec_error_d = self.aec_error_p - self.aec_error_p_mem[-1]
        self.aec_pid = self.aec_error_p * self.aec_p + self.alc_i * self.alc_error_i + self.aec_error_d * self.aec_d

        # See above for comments
        self.ccu_setting = int((1 + self.aec_pid) * self.ccu_setting_mem[-1])

        if self.ccu_setting > self.ccu_pos_lim:
            self.ccu_setting = self.ccu_pos_lim
        elif self.ccu_setting < self.ccu_neg_lim:
            self.ccu_setting = self.ccu_neg_lim

        # modify ccu setting to not set 17 or 19
        #self.ccu_setting = int(self.ccu_setting)
        #if self.ccu_setting == 17 or self.ccu_setting == 19:
        #    self.ccu_setting = 18

        # Update CCU
        self.ccu.SetShutterSync(self.ccu_setting)

    def update(self):

        # update memory
        self.img_pv_mem.append(self.img_pv)
        self.img_sat_mem.append(self.img_sat)
        self.img_error_target_mem.append(self.img_error_target)
        self.img_error_sat_mem.append(self.img_error_sat)
        self.alc_error_p_mem.append(self.alc_error_p)
        self.alc_error_i_mem.append(self.alc_error_i)
        self.alc_pid_mem.append(self.alc_pid)
        self.aec_error_p_mem.append(self.aec_error_p)
        self.aec_error_i_mem.append(self.aec_error_i)
        self.aec_pid_mem.append(self.aec_pid)
        self.led_setting_mem.append(self.led_setting)
        self.ccu_setting_mem.append(self.ccu_setting)
        self.sample += 1

        # print changes
        print('\nSample: %d' % self.sample)
        print('PV: ' + str(self.img_pv))
        print('Saturated: ' + str(self.img_sat))
        print('Target: ' + str(self.setpoint))

        if self.switch:
            print('\nALC Mode')
        else:
            print('\nAEC Mode')
        print('LED Positive Limit: ' + str(self.led_pos_limit))
        print('LED Negative Limit: ' + str(self.led_neg_limit))
        print('CCU Positive Limit: ' + str(self.ccu_pos_limit))
        print('CCU Negative Limit: ' + str(self.ccu_neg_limit))

        print('\nError from Target: ' + str(self.img_error_target))
        print('Error from Sat: ' + str(self.img_error_sat))
        print('Total Error input to LED: ' + str(self.alc_error_p))
        print('Total Error input to CCU: ' + str(self.aec_error_p))
        print('\nLED direction: ' + str(self.alc_pid))
        print('CCU direction: ' + str(self.aec_pid))

        # show image and wait 1 ms
        cv2.imshow('feed me!', self.img_show)
        cv2.waitKey(1)

        var_list = [getDateTime(), self.sample, self.setpoint, self.img_pv, self.img_error_target, self.img_error_sat,
                    self.b_sat, self.sat_weight, self.switch, not self.switch, self.led_setting, self.led_pos_limit,
                    self.led_neg_limit, self.ccu_setting, self.ccu_pos_limit, self.ccu_neg_limit, self.alc_p,
                    self.alc_i, self.alc_d, self.aec_p, self.aec_i, self.aec_d, self.alc_error_p, self.alc_error_i,
                    self.alc_error_d, self.aec_error_p, self.aec_error_i, self.aec_error_d]
        self.f.writerow(var_list)

        print('Shutter commanded to: ' + str(self.ccu_setting))
        print('LED set to: ' + str(self.led_setting))

    def run_alc(self):
        while self.run:
            # clear screen
            os.system('cls')
            if msvcrt.kbhit() != 0:
                k = msvcrt.getch()
                if k == b'q':
                    break
                elif k == b'w':
                    print('\nCurrent Setpoint is: %d' % self.setpoint)
                    self.setpoint = float(input('Select new setpoint (0-255): '))
            self.getImage()
            self.pv_switch()
            self.update()
            print('Refresh Rate: ' + str((np.round(1 / (tm.time() - self.last_time_sample), 2))) + ' Hz')
            self.last_time_sample = tm.time()
        cv2.destroyAllWindows()
        print('ALC closed')
