from ToshibaSerialCCU import ToshibaCCU
import time

print('\n****************************************************')
print('**************** Yampy\'s Auto White Balance ********')
print('****************************************************')

try:
    print('Setting Shutter to Auto')
    ccu = ToshibaCCU()
    ccu.SetShutterMode(0)
    ccu.SetShutterLevel(75)
    time.sleep(1)
    ccu.SetWbMode(0)
    ccu.SetWbCtemp(1)
    ccu.SetDoAwb()
    ccu.close()
    print('Auto White Balance Complete!! Push Enter to Exit...')
except:
    print('ERROR: Auto White Balance Failed!  Hit Enter to Exit...')

input()