import cv2
import numpy as np
import msvcrt
import os
import matplotlib.pyplot as plt
import time as tm
import imutils

from PhidgetClass import LED
from ToshibaClass import toshibaImage
from ToshibaSerialCCU import ToshibaCCU


class ALC:
    def __init__(self):
        self.led = LED()
        self.ccu = ToshibaCCU()
        self.vid = toshibaImage(1)
        self.ccu.SetShutterMode(2)

        self.response_memory = []
        self.led_control_memory = []
        self.p = []
        self.i = []
        self.d = []
        self.ccu_control_memory = []
        self.ccu_counter = 0

        self.setpoint = 0.65
        self.run = True

        #Keeps integral term from running away if CCU and LED are at limits!
        self.limit = False
        self.ccu_limit = False
        self.led_limit = False

        self.led_last = 10
        self.ccu_last = 218
        self.ccu.SetShutterSync(self.ccu_last)
        self.led.setVoltage(0, self.led_last)

        self.error_p = 0
        self.error_i = 0
        self.error_d = 0

        self.rot_count = 0

        self.last_time_sample = tm.time()

    def __del__(self):

        self.led.close()
        self.ccu.close()
        self.vid.close()
        print('Comms Closed')

    def getImage(self):

        self.img = self.vid.getImage()
        self.img = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
        self.img = self.img[110:330, 110:330]


    def pid(self):

        #Get Error
        self.error = np.round(self.setpoint - np.mean(self.img) / 255, 3)

        #Add to Response and Trim to 40
        self.response_memory.append(self.error)
        self.response_memory = self.response_memory[-40:]

        #Make sure at least 2 error samples in memory before adjusting controls
        self.primer = len(self.response_memory) > 2
        if self.primer == True:

            #calculate p i and d
            self.error_p = self.error
            if (not self.ccu_limit) or (not self.led_limit):
                self.error_i += self.error * (tm.time() - self.last_time_sample)
            self.error_d = (self.response_memory[-1] - self.response_memory[-2]) / (tm.time() - self.last_time_sample)
            self.p.append(self.error_p)
            self.i.append(self.error_i)
            self.d.append(self.error_d)

            led_p = 0.5
            led_i = 1.5
            led_d = 0
            self.led_control = led_p * self.error_p + led_i * self.error_i + led_d * self.error_d

            ccu_p = 0#15
            ccu_i = 0#7
            ccu_d = 0
            self.ccu_control = ccu_p * self.error_p + ccu_i * self.error_i + ccu_d * self.error_d

        else:
            self.led_control = 0
            self.ccu_control = 0

    def update(self):

        #clear screen
        os.system('cls')

        #print changes
        print('\nerror: ' + str(self.error))
        print('led control (how fast to change): ' + str(self.led_control))
        print('ccu control (how fast to change): ' + str(self.ccu_control))
        print('ccu I locked: '+str(self.ccu_limit))
        print('led I locked: '+str(self.led_limit))

        #show image and wait 1 ms
        cv2.imshow('feed me!', self.img_rot)
        cv2.waitKey(1)

        #Set LED resolution to 1 mV and add to current setting
        self.led_setting = np.round(self.led_last + self.led_control, 3)


        led_max = 10
        led_min = 0
        #If high limit, lock if positive error (needs more light)
        if self.led_setting > led_max:
            # Only engage lock if error is positive (meaning it needs more light) and light is maxed
            # Else, unlock limit (so I can stack in opposite error)
            if self.error > 0:
                self.led_limit = True
            else:
                self.led_limit = False

            # reset setting to max
            self.led_setting = led_max

        # If at low limit, lock if negative error (needs less light)
        elif self.led_setting < led_min:
            # Only engage lock if error is negative (meaning it needs less light) and light is minned
            # Else, unlock limit (so I can stack in opposite error)
            if self.error < 0:
                self.led_limit = True
            else:
                self.led_limit = False
            self.led_setting = led_min
        # else keep I unlocked
        else:
            # else keep I unlocked
            self.led_limit = False

        # Set current as last setting (so that PID can add RELATIVELY to current setting!)
        self.led_last = self.led_setting


        # See above for comments
        self.ccu_setting = self.ccu_last + self.ccu_control

        ccu_max = 218
        ccu_min = 1
        if self.ccu_setting > ccu_max:
            if self.error > 0:
                self.ccu_limit = True
            else:
                self.ccu_limit = False
            self.ccu_setting = ccu_max
        elif self.ccu_setting < ccu_min:
            if self.error < 0:
                self.ccu_limit = True
            else:
                self.ccu_limit = False
            self.ccu_setting = ccu_min
        else:
            self.ccu_limit = False

        self.ccu_last = self.ccu_setting


        # modify ccu setting to not set 17 or 19
        self.ccu_setting = int(self.ccu_setting)
        if self.ccu_setting == 17 or self.ccu_setting == 19:
            self.ccu_setting = 18


        # change CCU setting once every 10 cycles
        '''
        if self.ccu_counter == 4:
            self.ccu.SetShutterSync(self.ccu_setting)
            self.ccu_counter = 0
        else:
            self.ccu_counter += 1
        '''

        #Update CCU and LED
        #self.ccu.SetShutterSync(self.ccu_setting)
        self.led.setVoltage(0, self.led_setting)

        #Write and trim LED and CCU memories
        self.led_control_memory.append(self.led_setting / 10)
        self.led_control_memory = self.led_control_memory[-40:]
        self.ccu_control_memory.append(self.ccu_setting / 218)
        self.ccu_control_memory = self.ccu_control_memory[-40:]

        self.p = self.p[-40:]
        self.i = self.i[-40:]
        self.d = self.d[-40:]

        print('Shutter commanded to: ' + str(self.ccu_setting))
        # print('Shutter Set to: ' + str(self.ccu.GetShutterSync()))
        print('LED set to: ' + str(self.led_setting))

    def run_alc(self):
        while self.run:
            if msvcrt.kbhit() != 0:
                k = msvcrt.getch()
                if k == b'q':
                    break
            self.getImage()
            self.pid()
            self.update()
            print('Period: ' + str((np.round(1 / (tm.time() - self.last_time_sample), 2))) + ' Hz')
            self.last_time_sample = tm.time()
        cv2.destroyAllWindows()


        f, axarr = plt.subplots(6, sharex=True)
        axarr[0].plot(self.response_memory, color='b', marker=r'$\lambda$', markersize = 10)
        axarr[0].set_title('Error')
        axarr[1].plot(self.led_control_memory, color='g', marker=r'$\bowtie$', markersize = 10)
        axarr[1].set_title('LED Control')
        axarr[2].plot(self.ccu_control_memory, color='r', marker=r'$\circlearrowleft$', markersize = 10)
        axarr[2].set_title('CCU Control')
        axarr[3].plot(self.p, label='P', color='c', marker=r'$\clubsuit$', markersize = 10)
        axarr[3].set_title('P')
        axarr[4].plot(self.i, label='I', color='m', marker=r'$\checkmark$', markersize = 10)
        axarr[4].set_title('I')
        axarr[5].plot(self.d, label='D', color='y', marker = 'o', markersize = 10)
        axarr[5].set_title('D')
        plt.show()

        print('ALC closed')


banana = ALC()
banana.run_alc()
