import cv2
import numpy as np
import msvcrt
import os
import matplotlib.pyplot as plt
import time as tm
from collections import deque

from PhidgetClass import LED
from ToshibaClass import toshibaImage
from ToshibaSerialCCU import ToshibaCCU


class ALC:
    def __init__(self):
        self.led = LED()
        self.ccu = ToshibaCCU()
        self.vid = toshibaImage(1)
        self.ccu.SetShutterMode(2)

        d_len = 40
        self.img_pv_mem = deque(maxlen=d_len)
        self.img_sat_mem = deque(maxlen=d_len)
        self.img_error_target_mem = deque(maxlen=d_len)
        self.img_error_sat_mem = deque(maxlen=d_len)
        self.alc_error_p_mem = deque(maxlen=d_len)
        self.alc_error_i_mem = deque(maxlen=d_len)
        self.alc_pid_mem = deque(maxlen=d_len)
        self.aec_error_p_mem = deque(maxlen=d_len)
        self.aec_error_i_mem = deque(maxlen=d_len)
        self.aec_pid_mem = deque(maxlen=d_len)
        self.led_control_mem = deque(maxlen=d_len)
        self.led_setting_mem = deque(maxlen=d_len)
        self.ccu_control_mem = deque(maxlen=d_len)
        self.ccu_setting_mem = deque(maxlen=d_len)

        # Target and run condition
        self.setpoint = 120
        self.run = True

        # Keeps integral term from running away if CCU and LED are at limits!
        self.limit = False
        self.ccu_limit = False
        self.led_limit = False

        # Initial LED/CCU settings
        self.led_setting = 10
        self.ccu_setting = 218
        self.ccu.SetShutterSync(self.ccu_setting)
        self.led.setVoltage(0, self.led_setting)

        # Initial Error settings for primer
        self.alc_error_p = 0
        self.alc_error_i = 0
        self.alc_error_d = 0
        self.alc_pid = 0
        self.aec_error_p = 0
        self.aec_error_i = 0
        self.aec_error_d = 0
        self.aec_pid = 0

        # Light/Exposure initials
        self.switch = True  # fixed exposure if True, fixed led if False
        self.sat_weight = 240
        self.b_sat = 920

        self.alc_p = 0.25
        self.alc_i = 0.001
        self.alc_d = 0.0025
        self.aec_p = 0.25
        self.aec_i = 0  # 0.001
        self.aec_d = 0.0025

        self.last_time_sample = tm.time()

        # other useless inits
        self.img = None
        self.img_pv = 0
        self.img_sat = 0
        self.img_error_target = 0
        self.img_error_sat = 0
        self.primer = True

    def __del__(self):

        self.led.close()
        self.ccu.close()
        self.vid.close()
        print('Comms Closed')

    def getImage(self):

        self.img = self.vid.getImage()
        self.img_show = self.img[40:400, 40:400]
        cv2.rectangle(self.img_show, (30, 30), (330, 330), (255, 255, 0), 2)
        self.img = cv2.cvtColor(self.img_show, cv2.COLOR_BGR2GRAY)
        self.img = self.img[30:330, 30:330]

    def pv_switch(self):

        # Get PV (0-255)
        self.img_pv = np.round(np.mean(self.img),3)
        # Calculate number of sat pixels
        self.img_sat = len(np.where(self.img == 255)[0])

        # Calculate image and saturation error
        self.img_error_target = (self.setpoint - self.img_pv) / self.setpoint
        self.img_error_sat = -self.sat_weight * self.img_sat / (self.img_pv * pow(self.b_sat - self.img_pv, 2))

        # if operating in ALC, fixed exposure

        # Make sure at least 2 error samples in memory before adjusting controls
        self.primer = len(self.img_pv_mem) > 10
        if self.primer:
            # Fixed Exposure

            if self.switch:
                self.alc_pid_calc()

                # if led is low and cannot go lower switch to fixed LED with low LED, switch back to fixed LED
                if self.led_setting < 1.0 and self.img_error_target < 0:
                    self.switch = False

                # reset exposure to max if not
                if not self.ccu_setting_mem[-1] == 217:
                    self.ccu_setting = 217
                    self.ccu.SetShutterSync(217)

                #if self.led_setting == 10 and self.img_error_target > 0 and not self.ccu_limit:
                #    self.ccu_limit = True


            # Fixed LED
            else:
                self.aec_pid_calc()

                # if CCU exposure is lowest and still need more light assuming still in low LED mode, switch back to fixed exposure
                if self.ccu_setting_mem[-1] > 180 and self.img_error_target > 0:
                    self.switch = True

                # reset light to min if not
                if self.led_setting_mem[-1] > 1.0:
                    self.led_setting = 1.0
                    self.led.setVoltage(0, 1.0)

    def alc_pid_calc(self):

        self.alc_error_p = self.img_error_target + self.img_error_sat
        self.alc_error_i = 0.5 * self.alc_error_i_mem[-1] + self.alc_error_p
        self.alc_error_d = self.alc_error_p - self.alc_error_p_mem[-1]
        self.alc_pid = self.alc_p * self.alc_error_p + self.alc_i * self.alc_error_i + self.alc_d * self.alc_error_d

        # Set LED resolution to 1 mV and add to current setting
        self.led_setting = np.round((1 + self.alc_pid) * self.led_setting_mem[-1], 3)

        led_max = 10
        led_min = 0

        if self.led_setting > led_max:
            self.led_setting = led_max
        elif self.led_setting < led_min:
            self.led_setting = led_min

        '''
        #If high limit, lock if positive error (needs more light)
        if self.led_setting > led_max:
            # Only engage lock if error is positive (meaning it needs more light) and light is maxed
            # Else, unlock limit (so I can stack in opposite error)
            if self.alc_error_p > 0:
                self.led_limit = True
            else:
                self.led_limit = False

            # reset setting to max
            self.led_setting = led_max

        # If at low limit, lock if negative error (needs less light)
        elif self.led_setting < led_min:
            # Only engage lock if error is negative (meaning it needs less light) and light is minned
            # Else, unlock limit (so I can stack in opposite error)
            if self.alc_error_p < 0:
                self.led_limit = True
            else:
                self.led_limit = False
            self.led_setting = led_min
        # else keep I unlocked
        else:
            # else keep I unlocked
            self.led_limit = False
        '''

        # Update CCU and LED
        self.led.setVoltage(0, self.led_setting)


    def aec_pid_calc(self):

        self.aec_error_p = self.img_error_target + self.img_error_sat
        self.aec_error_i = 0.5 * self.aec_error_i_mem[-1] + self.aec_error_p
        self.aec_error_d = self.aec_error_p - self.aec_error_p_mem[-1]
        self.aec_pid = self.aec_error_p * self.aec_p + self.alc_i * self.alc_error_i + self.aec_error_d * self.aec_d

        # See above for comments
        self.ccu_setting = int((1 + self.aec_pid) * self.ccu_setting_mem[-1])

        ccu_max = 218
        ccu_min = 10

        if self.ccu_setting > ccu_max:
            self.ccu_setting = ccu_max
        elif self.ccu_setting < ccu_min:
            self.ccu_setting = ccu_min

        '''
        if self.ccu_setting > ccu_max:
            if self.aec_error_p > 0:
                self.ccu_limit = True
            else:
                self.ccu_limit = False
            self.ccu_setting = ccu_max
        elif self.ccu_setting < ccu_min:
            if self.aec_error_p < 0:
                self.ccu_limit = True
            else:
                self.ccu_limit = False
            self.ccu_setting = ccu_min
        else:
            self.ccu_limit = False
        '''

        # modify ccu setting to not set 17 or 19
        self.ccu_setting = int(self.ccu_setting)
        if self.ccu_setting == 17 or self.ccu_setting == 19:
            self.ccu_setting = 18

        # Update CCU
        self.ccu.SetShutterSync(self.ccu_setting)


    def update(self):

        # update memory
        self.img_pv_mem.append(self.img_pv)
        self.img_sat_mem.append(self.img_sat)
        self.img_error_target_mem.append(self.img_error_target)
        self.img_error_sat_mem.append(self.img_error_sat)
        self.alc_error_p_mem.append(self.alc_error_p)
        self.alc_error_i_mem.append(self.alc_error_i)
        self.alc_pid_mem.append(self.alc_pid)
        self.aec_error_p_mem.append(self.aec_error_p)
        self.aec_error_i_mem.append(self.aec_error_i)
        self.aec_pid_mem.append(self.aec_pid)
        self.led_setting_mem.append(self.led_setting)
        self.ccu_setting_mem.append(self.ccu_setting)

        # clear screen
        os.system('cls')

        # print changes
        print('\nPV: ' + str(self.img_pv))
        print('Saturated: ' + str(self.img_sat))
        print('Target: '+str(self.setpoint))

        print('\nError from Target: ' + str(self.img_error_target))
        print('Error from Sat: ' + str(self.img_error_sat))
        print('Total Error input to LED: ' + str(self.alc_error_p))
        print('Total Error input to CCU: ' + str(self.aec_error_p))
        print('\nLED direction: ' + str(self.alc_pid))
        print('CCU direction: ' + str(self.aec_pid))

        # show image and wait 1 ms
        cv2.imshow('feed me!', self.img_show)
        cv2.waitKey(1)

        print('Shutter commanded to: ' + str(self.ccu_setting))
        print('LED set to: ' + str(self.led_setting))

    def run_alc(self):
        while self.run:
            if msvcrt.kbhit() != 0:
                k = msvcrt.getch()
                if k == b'q':
                    break
            self.getImage()
            self.pv_switch()
            self.update()
            print('Refresh Rate: ' + str((np.round(1 / (tm.time() - self.last_time_sample), 2))) + ' Hz')
            self.last_time_sample = tm.time()
        cv2.destroyAllWindows()

        '''
        f, axarr = plt.subplots(6, sharex=True)
        axarr[0].plot(self.response_memory, color='b', marker=r'$\lambda$', markersize = 10)
        axarr[0].set_title('Error')
        axarr[1].plot(self.led_control_memory, color='g', marker=r'$\bowtie$', markersize = 10)
        axarr[1].set_title('LED Control')
        axarr[2].plot(self.ccu_control_memory, color='r', marker=r'$\circlearrowleft$', markersize = 10)
        axarr[2].set_title('CCU Control')
        axarr[3].plot(self.p, label='P', color='c', marker=r'$\clubsuit$', markersize = 10)
        axarr[3].set_title('P')
        axarr[4].plot(self.i, label='I', color='m', marker=r'$\checkmark$', markersize = 10)
        axarr[4].set_title('I')
        axarr[5].plot(self.d, label='D', color='y', marker = 'o', markersize = 10)
        axarr[5].set_title('D')
        plt.show()
        '''

        print('ALC closed')


banana = ALC()
banana.run_alc()
